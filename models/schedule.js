const mongoose = require('mongoose');

const studioSchedule =new mongoose.Schema({
    startTime:{
        type:String
    },
    endTime:{
        type:String
    },
    faculty:{
        type:String
    },
    assignerName:{
        type:String
    },

});
const studiobyDate =new mongoose.Schema({
    localDate:{
        type:String
    },
    schedulebyTime:[studioSchedule]

});
const scheduleSchema =new mongoose.Schema({
    studioName:{
        type:String
    },
    schedule:[studiobyDate]

},{timestamps:true},{ versionKey: false });

const Schedule =mongoose.model('Schedule',scheduleSchema);
module.exports=Schedule