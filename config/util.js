const jwt = require("jsonwebtoken");
const Role  = require("../models/roles");
const Privilage  = require("../models/privileges");

const checkToken = (req, res, next) => {

  retriveToken(req).then(token => {
   jwt.verify(token, instance.secret, (err, authorizedData) => {
    if (err) {
      console.log(token);
      if (err.message === "jwt expired") {
        res.json({success: false, message: err})
      } else {
        res.json({success: false, message: err})
      }
    }
    else{
     
      res.json({success: true, user: authorizedData})

    }
  });
  })
  .catch(err => {
    console.log(err);
    res.json({success: false, message: err})
  });
};

const checkAccess = (req, res, next) => {
  console.log(req.method,req.originalUrl);
 
   retriveToken(req).then(token => {
   // console.log(token);
    checkRole(token,req.method,req.originalUrl).then(() => {
     next();
    })
     .catch(err => {
       console.log(err);
       res.json({success: false, message: err})
     })
   })
   .catch(err => {
     console.log(err);
     res.json({success: false, message: err})
   })
 };

const retriveToken = req => {
 return new Promise((resolve, reject) => {
    const header = req.headers["authorization"];
    if (header) {
      const bearer = header.split(" ");
      const token = bearer[1];
      if (token) {
       resolve(token);
      } else {
      reject("Access token invalid" );
      }
    } else {
      reject("Unauthorized");
    }
  });
};


const checkRole = (token,method,url) => {
  return new Promise((resolve, reject) => {
  jwt.verify(token, instance.secret, (err, authorizedData) => {
    if (err) {
      // console.log(err);
      if (err.message === "jwt expired") {
        reject("Session expired");
      } else {
       reject(err.message);
      }
    } else {
      console.log(authorizedData.roleId,authorizedData._id);
     
      if(method==="DELETE"){
       var trimUrl=url.substr(0, url.lastIndexOf("/"));
        Privilage.findOne({endPoint:trimUrl,httpMethod:method})
        .then(priv=>{
          console.log(priv);
          Role.find({_id:authorizedData.roleId,privileges:{"$in":[priv._id]}})
          .then(access=>{
            console.log(access);
            if(access!==[]){
              resolve();
            }
            else{
              reject("access denied") 
            }
           })
        })
      }
      else{
        Privilage.findOne({endPoint:url,httpMethod:method})
        .then(priv=>{
          if(priv!==null){
          console.log("priv"+priv);
          Role.findOne({_id:authorizedData.roleId,privileges:{"$in":[priv._id]}})
          .then(access=>{
            console.log("access"+access);
            if(access!==null){
              resolve();
            }
            else{
              reject("access denied") 
            }
          })
          .catch(err => {
            console.log(err);
            res.json({success: false, message: err})
          })
        }
        else{
          reject("Privilage not found") 

        }
      })
      }


      

    }
  });
});
}





module.exports = { checkAccess,checkToken };
