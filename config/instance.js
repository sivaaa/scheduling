'use strict';

const instanceName = process.env.NODE_ENV || 'prod';

switch (instanceName) {
    case "dev":
        {   
            exports.instance = "dev"
            exports.port = 5002;
            exports.secret = "poijasdf98435jpgfdpoij3";
            exports.dbUrl = "mongodb://localhost:27017/CloudDev";

        }
        break;
    case "prod":
        {
            exports.instance = "prod"
            exports.port = 5000;
            exports.secret = "pojiaj234oi234oij234oij4";
            exports.dbUrl = "mongodb://localhost:27017/Schedule";
        }
        break;
    case "qa":
        {
            exports.instance = "qa"
            exports.port = 5001;
            exports.secret = "poijasdf98435jpgfdpoij3";
            exports.dbUrl = "mongodb://localhost:27017/CloudQA";
        }   
        break;
}