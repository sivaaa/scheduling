// app.js
const express = require('express')
var bodyParser = require('body-parser')

/************* mongoose setup ***************/
require("./db/mongoose");
instance = require("./config/instance");

var cors = require('cors');

// Create Express app
const app = express();
var apirouter=require('./routes/apirouter')


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Expose-Headers" ,"x-auth");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-type,Accept, Authorization"
    );
    if (req.method == "OPTIONS") {
      res.status(200).end();
    } else {
      next();
    }
  });
  

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
app.use('/',apirouter)

// A sample route
app.get('/', function(req, res){res.send('Hello World!')} )




// Start the Express server
app.listen(instance.port, function() {
    console.log('Server running on port' +instance.port)
} )  